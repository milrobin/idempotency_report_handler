name             'idempotency_report_handler'
maintainer       'Anthony Robinson'
maintainer_email 'milrobin@starbucks.com'
license          'All rights reserved'
description      'Installs/Configures a report handler to report applied resources at end of Chef run.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'chef_handler'
