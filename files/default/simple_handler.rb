require 'chef/handler'

class Chef
  class Handler

    class SimpleHandler < Chef::Handler
      def report
	      changed = 0
	      Chef::Log.warn "Resources updated this run:"
#		require 'pry';binding.pry
	      run_status.updated_resources.each {|r| 
		index = "****"
		if r.cookbook_name == 'idempotency_report_handler' || r.name  =~ /^KITCHEN-/
			index = "****"
		else
			changed += 1
			index = "%4.4s" % changed	
		end
		Chef::Log.warn "#{index} #{r.cookbook_name}:#{r.recipe_name}:#{r.to_s}"
	      }
	      Chef::Log.warn "#{'%4.4s' % changed} Total resources changed"
	      result = changed == 0 ? "IDEMPOTENT" : "CHANGES MADE"
              Chef::Log.warn "**** #{result}"
      end
    end
  end
end
