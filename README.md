Idempotency Report Handler Cookbook
=======================
This cookbook installs a simple report handler which reports a list of applied resources in a run.

To incorporate it, include the default recipe in the Chef run.

The changes made by this cookbook aren't included in the changes made.

Any resource with a name beginning with 'KITCHEN-' is also excluded from the count.

The summary will indicate IDEMPOTENT if there are no other changes than the above, else it will indicate CHANGES.

## To demo a report handler
Simply run `kitchen converge`.

Should see a WARN output like this:
```
       Running handlers:
       [2016-11-08T12:00:37-08:00] WARN: Resources updated this run:
       [2016-11-08T12:00:37-08:00] WARN: **** idempotency_report_handler:default:cookbook_file[C:/Users/vagrant/AppData/Local/Temp/kitchen/handlers/simple_handler.rb]
       [2016-11-08T12:00:37-08:00] WARN: **** idempotency_report_handler:default:chef_handler[Chef::Handler::SimpleHandler]
       [2016-11-08T12:00:37-08:00] WARN: **** gpe-chef-base:_test:ruby_block[KITCHEN-Save node attributes]
       [2016-11-08T12:00:37-08:00] WARN:    0 Total resources changed
       [2016-11-08T12:00:37-08:00] WARN: **** IDEMPOTENT
```

## To demo a failure handler
Edit the default recipe to uncomment the fail method in the `fail the run` ruby_block and run `kitchen converge`.

Should see a WARN output like this:
```
Running handlers:
[2016-09-15T12:49:09-07:00] ERROR: Running exception handlers
[2016-09-15T12:49:09-07:00] WARN: SimpleHandler: @run_status.success? => false
- Chef::Handler::SimpleHandler
Running handlers complete
```
